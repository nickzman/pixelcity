//
//  PixelCityOpenGLView.m
//  PixelTesterApp
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright 2009 Electronic Magicians HB. All rights reserved.
//

#import "PixelCityOpenGLView.h"

#include "Win.h"
#include "glTypes.h"
#include "Render.h"

@implementation PixelCityOpenGLView

#define MyModuleName @"se.emage.PixelCitySaver"

-(void)prepare
{
  NSOpenGLContext* glContext = [self openGLContext];
  [glContext makeCurrentContext];
  
  worldState.view = self;
  AppInit(&worldState);
}


-(id)initWithCoder:(NSCoder*)coder
{
  self = [super initWithCoder:coder];
  
  [self prepare];
  
  return self;
}


- (id)initWithFrame:(NSRect)frame pixelFormat:(NSOpenGLPixelFormat*)pixelFormat 
{
  
  self = [super initWithFrame:frame pixelFormat:pixelFormat];
  if (self) {
    // Initialization code here.
       
  }
  
  return self;
}


- (id)initWithFrame:(NSRect)frame 
{
  NSOpenGLPixelFormatAttribute att[] = 
	{
		NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFAColorSize, 24,
		NSOpenGLPFAAlphaSize, 8,
		NSOpenGLPFADepthSize, 24,
		NSOpenGLPFANoRecovery,
		NSOpenGLPFAAccelerated,
		0
	};
  
  NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:att];
  
  self = [super initWithFrame:frame pixelFormat:pixelFormat];
  if (self) {
      // Initialization code here.
    [self prepare];
    
    redrawTimer = [NSTimer scheduledTimerWithTimeInterval:1/60.0f target:self selector:@selector(redrawCB:) userInfo:nil repeats:YES];
  }
  return self;
}


-(void)reshape
{
  RenderResize(&worldState);
}

-(void)redrawCB:(void*)data
{
  [self setNeedsDisplay:YES];
}


- (void)drawRect:(NSRect)rect 
{
  NSOpenGLContext* glContext = [self openGLContext];
  [glContext makeCurrentContext];

  AppUpdate(&worldState);
  glFinish();
  [glContext flushBuffer];
  
}

@end
