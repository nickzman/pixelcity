//
//  PixelCityOpenGLView.h
//  PixelTesterApp
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright 2009 Electronic Magicians HB. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "State.h"


@interface PixelCityOpenGLView : NSOpenGLView {
  NSTimer* redrawTimer;
  state_t worldState;
  
}

@end
