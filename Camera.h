#ifndef TYPES
#include "glTypes.h"
#endif

#include "State.h"

GLvector  CameraAngle (state_t* state);
void      CameraAngleSet (state_t* state, GLvector new_angle);
void      CameraAutoToggle (state_t* state);
float     CameraDistance (state_t* state);
void      CameraDistanceSet (state_t* state, float new_distance);
void      CameraInit (state_t* state);
void      CameraNextBehavior (state_t* state);
GLvector  CameraPosition (state_t* state);
void      CameraPositionSet (state_t* state, GLvector new_pos);
void      CameraReset (state_t* state);
void      CameraUpdate (state_t* state);	
void      CameraTerm (state_t* state);

void      CameraForward (state_t* state, float delta);
void      CameraSelectionPitch (state_t* state, float delta_y);
void      CameraSelectionYaw (state_t* state, float delta_x);
void      CameraSelectionZoom (state_t* state, float delta_y);
void      CameraPan (state_t* state, float delta_x);
void      CameraPitch (state_t* state, float delta_y);
void      CameraYaw (state_t* state, float delta_x);
