//
//  pixelcityView.h
//  pixelcity
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright (c) 2009, Electronic Magicians HB. All rights reserved.
//

#import <ScreenSaver/ScreenSaver.h>


@interface pixelcityView : ScreenSaverView 
{
  IBOutlet id configSheet;
  
  IBOutlet id letterboxOption;
  IBOutlet id flatOption;
  
  IBOutlet id bloomOption;
  IBOutlet id wireframeOption;
  IBOutlet id fogOption;

}

- (IBAction)cancelClick:(id)sender;
- (IBAction)okClick:(id)sender;

@end
