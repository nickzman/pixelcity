/*-----------------------------------------------------------------------------

  Render.cpp

  2009 Shamus Young

-------------------------------------------------------------------------------
  
  This is the core of the gl rendering functions.  This contains the main 
  rendering function RenderUpdate (), which initiates the various 
  other renders in the other modules. 

-----------------------------------------------------------------------------*/

#define RENDER_DISTANCE     1280
#define MAX_TEXT            256
#define YOUFAIL(message)    {WinPopup (message);return;}
#define HELP_SIZE           sizeof(help)
#define COLOR_CYCLE_TIME    10000 //milliseconds
#define COLOR_CYCLE         (COLOR_CYCLE_TIME / 4)
#define FONT_COUNT          (sizeof (fonts) / sizeof (struct glFont))
#define FONT_SIZE           (LOGO_PIXELS - LOGO_PIXELS / 8)
#define BLOOM_SCALING       0.07f

#ifdef _WIN32
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#else
#include <GLUT/GLUT.h>
#include <stdlib.h>
#include <string.h>
#endif
#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>


#include "gltypes.h"
#include "Entity.h"
#include "Car.h"
#include "Camera.h"
#include "Ini.h"
#include "Light.h"
#include "Macro.h"
#include "PixelMath.h"
#include "Render.h"
#include "Sky.h"
#include "Texture.h"
#include "World.h"
#include "Win.h"

#ifdef _WIN32
static	PIXELFORMATDESCRIPTOR pfd =			
{
	sizeof(PIXELFORMATDESCRIPTOR),			
	1,											  // Version Number
	PFD_DRAW_TO_WINDOW |			// Format Must Support Window
	PFD_SUPPORT_OPENGL |			// Format Must Support OpenGL
	PFD_DOUBLEBUFFER,					// Must Support Double Buffering
	PFD_TYPE_RGBA,						// Request An RGBA Format
	32,										    // Select Our glRgbaDepth
	0, 0, 0, 0, 0, 0,					// glRgbaBits Ignored
	0,											  // No Alpha Buffer
	0,											  // Shift Bit Ignored
	0,											  // Accumulation Buffers
	0, 0, 0, 0,								// Accumulation Bits Ignored
	16,											  // Z-Buffer (Depth Buffer)  bits
	0,											  // Stencil Buffers
	1,											  // Auxiliary Buffers
	PFD_MAIN_PLANE,						// Main Drawing Layer
	0,											  // Reserved
	0, 0, 0										// Layer Masks Ignored
};
#endif // ifdef _WIN32

static char             help[] = 
  "ESC - Exit!\n" 
  "F1  - Show this help screen\n" 
  "R   - Rebuild city\n" 
  "L   - Toggle 'letterbox' mode\n"
  "F   - Show Framecounter\n"
  "W   - Toggle Wireframe\n"
  "E   - Change full-scene effects\n"
  "T   - Toggle Textures\n"
  "G   - Toggle Fog\n"
;

struct glFont
{
  char*         name;
  unsigned		  base_char;
} fonts[] = 
{
  "Courier New",      0,
  "Arial",            0,
  "Times New Roman",  0,
  "Arial Black",      0,
  "Impact",           0,
  "Agency FB",        0,
  "Book Antiqua",     0,
};

#if SCREENSAVER
enum
{
  EFFECT_NONE,
  EFFECT_BLOOM,
  EFFECT_BLOOM_RADIAL,
  EFFECT_COLOR_CYCLE,
  EFFECT_GLASS_CITY,
  EFFECT_COUNT,
  EFFECT_DEBUG,
  EFFECT_DEBUG_OVERBLOOM,
};
#else
enum
{
  EFFECT_NONE,
  EFFECT_BLOOM,
  EFFECT_COUNT,
  EFFECT_DEBUG_OVERBLOOM,
  EFFECT_DEBUG,
  EFFECT_BLOOM_RADIAL,
  EFFECT_COLOR_CYCLE,
  EFFECT_GLASS_CITY,
};
#endif 


/*-----------------------------------------------------------------------------

  Draw a clock-ish progress.. widget... thing.  It's cute.

-----------------------------------------------------------------------------*/

static void do_progress (state_t* state, float center_x, float center_y, float radius, float opacity, float progress)
{

  int     i;
  int     end_angle;
  float   inner, outer;
  float   angle;
  float   s, c;
  float   gap;

  //Outer Ring
  gap = radius * 0.05f;
  outer = radius;
  inner = radius - gap * 2;
  glColor4f (1,1,1, opacity);
  glBegin (GL_QUAD_STRIP);
  for (i = 0; i <= 360; i+= 15) {
    angle = (float)i * DEGREES_TO_RADIANS;
    s = sinf (angle);
    c = -cosf (angle);
    glVertex2f (center_x + s * outer, center_y + c * outer);
    glVertex2f (center_x + s * inner, center_y + c * inner);
  }
  glEnd ();
  //Progress indicator
  glColor4f (1,1,1, opacity);
  end_angle = (int)(360 * progress);
  outer = radius - gap * 3;
  glBegin (GL_TRIANGLE_FAN);
  glVertex2f (center_x, center_y);
  for (i = 0; i <= end_angle; i+= 3) {
    angle = (float)i * DEGREES_TO_RADIANS;
    s = sinf (angle);
    c = -cosf (angle);
    glVertex2f (center_x + s * outer, center_y + c * outer);
  }
  glEnd ();
  //Tic lines
  glLineWidth (2.0f);
  outer = radius - gap * 1;
  inner = radius - gap * 2;
  glColor4f (0,0,0, opacity);
  glBegin (GL_LINES);
  for (i = 0; i <= 360; i+= 15) {
    angle = (float)i * DEGREES_TO_RADIANS;
    s = sinf (angle);
    c = -cosf (angle);
    glVertex2f (center_x + s * outer, center_y + c * outer);
    glVertex2f (center_x + s * inner, center_y + c * inner);
  }
  glEnd ();

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

static void do_effects (state_t* state, int type)
{

  float           hue1, hue2, hue3, hue4;
  GLrgba          color;
  float           fade;
  int             radius;
  int             x, y;
  int             i;
  int             bloom_radius;
  int             bloom_step;
  
  fade = WorldFade (state);
  bloom_radius = 9;
  bloom_step = bloom_radius / 3;
  if (!TextureReady (state))
    return;
  //Now change projection modes so we can render full-screen effects
  glMatrixMode (GL_PROJECTION);
  glPushMatrix ();
  glLoadIdentity ();
  glOrtho (0, state->render.render_width, state->render.render_height, 0, 0.1f, 2048);
	glMatrixMode (GL_MODELVIEW);
  glPushMatrix ();
  glLoadIdentity();
  glTranslatef(0, 0, -1.0f);				
  glDisable (GL_CULL_FACE);
  glDisable (GL_FOG);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  //Render full-screen effects
  glBlendFunc (GL_ONE, GL_ONE);
  glEnable (GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
  glDepthMask (false);
  glBindTexture(GL_TEXTURE_2D, TextureId (state, TEXTURE_BLOOM));
  switch (type) {
  case EFFECT_DEBUG:
    glBindTexture(GL_TEXTURE_2D, TextureId (state, TEXTURE_LOGOS));
    glDisable (GL_BLEND);
    glBegin (GL_QUADS);
    glColor3f (1, 1, 1);
    glTexCoord2f (0, 0);  glVertex2i (0, state->render.render_height / 4);
    glTexCoord2f (0, 1);  glVertex2i (0, 0);
    glTexCoord2f (1, 1);  glVertex2i (state->render.render_width / 4, 0);
    glTexCoord2f (1, 0);  glVertex2i (state->render.render_width / 4, state->render.render_height / 4);

    glTexCoord2f (0, 0);  glVertex2i (0, 512);
    glTexCoord2f (0, 1);  glVertex2i (0, 0);
    glTexCoord2f (1, 1);  glVertex2i (512, 0);
    glTexCoord2f (1, 0);  glVertex2i (512, 512);
    glEnd ();
    break;
  case EFFECT_BLOOM_RADIAL:
    //Psychedelic bloom
    glEnable (GL_BLEND);
    glBegin (GL_QUADS);
    color = WorldBloomColor (state) * BLOOM_SCALING * 2;
    glColor3fv (&color.red);
    for (i = 0; i <= 100; i+=10) {
      glTexCoord2f (0, 0);  glVertex2i (-i, i + state->render.render_height);
      glTexCoord2f (0, 1);  glVertex2i (-i, -i);
      glTexCoord2f (1, 1);  glVertex2i (i + state->render.render_width, -i);
      glTexCoord2f (1, 0);  glVertex2i (i + state->render.render_width, i + state->render.render_height);
    }
    glEnd ();
    break;
  case EFFECT_COLOR_CYCLE:
    //Oooh. Pretty colors.  Tint the scene according to screenspace.
    hue1 = (float)(GetTickCount () % COLOR_CYCLE_TIME) / COLOR_CYCLE_TIME;
    hue2 = (float)((GetTickCount () + COLOR_CYCLE) % COLOR_CYCLE_TIME) / COLOR_CYCLE_TIME;
    hue3 = (float)((GetTickCount () + COLOR_CYCLE * 2) % COLOR_CYCLE_TIME) / COLOR_CYCLE_TIME;
    hue4 = (float)((GetTickCount () + COLOR_CYCLE * 3) % COLOR_CYCLE_TIME) / COLOR_CYCLE_TIME;
    glBindTexture(GL_TEXTURE_2D, 0);
    glEnable (GL_BLEND);
    glBlendFunc (GL_ONE, GL_ONE);
    glBlendFunc (GL_DST_COLOR, GL_SRC_COLOR);
    glBegin (GL_QUADS);
    color = glRgbaFromHsl (hue1, 1.0f, 0.6f);
    glColor3fv (&color.red);
    glTexCoord2f (0, 0);  glVertex2i (0, state->render.render_height);
    color = glRgbaFromHsl (hue2, 1.0f, 0.6f);
    glColor3fv (&color.red);
    glTexCoord2f (0, 1);  glVertex2i (0, 0);
    color = glRgbaFromHsl (hue3, 1.0f, 0.6f);
    glColor3fv (&color.red);
    glTexCoord2f (1, 1);  glVertex2i (state->render.render_width, 0);
    color = glRgbaFromHsl (hue4, 1.0f, 0.6f);
    glColor3fv (&color.red);
    glTexCoord2f (1, 0);  glVertex2i (state->render.render_width, state->render.render_height);
    glEnd ();
    break;
  case EFFECT_BLOOM:
    //Simple bloom effect
    glBegin (GL_QUADS);
    color = WorldBloomColor (state) * BLOOM_SCALING;
    glColor3fv (&color.red);
    for (x = -bloom_radius; x <= bloom_radius; x += bloom_step) {
      for (y = -bloom_radius; y <= bloom_radius; y += bloom_step) {
        if (abs (x) == abs (y) && x)
          continue;
        glTexCoord2f (0, 0);  glVertex2i (x, y + state->render.render_height);
        glTexCoord2f (0, 1);  glVertex2i (x, y);
        glTexCoord2f (1, 1);  glVertex2i (x + state->render.render_width, y);
        glTexCoord2f (1, 0);  glVertex2i (x + state->render.render_width, y + state->render.render_height);
      }
    }
    glEnd ();
    break;
  case EFFECT_DEBUG_OVERBLOOM:
    //This will punish that uppity GPU. Good for testing low frame rate behavior.
    glBegin (GL_QUADS);
    color = WorldBloomColor (state) * 0.01f;
    glColor3fv (&color.red);
    for (x = -50; x <= 50; x+=5) {
      for (y = -50; y <= 50; y+=5) {
        glTexCoord2f (0, 0);  glVertex2i (x, y + state->render.render_height);
        glTexCoord2f (0, 1);  glVertex2i (x, y);
        glTexCoord2f (1, 1);  glVertex2i (x + state->render.render_width, y);
        glTexCoord2f (1, 0);  glVertex2i (x + state->render.render_width, y + state->render.render_height);
      }
    }
    glEnd ();
    break;
  }
  //Do the fade to / from darkness used to hide scene transitions
  if (LOADING_SCREEN) {
    if (fade > 0.0f) {
    
      glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEnable (GL_BLEND);
      glDisable (GL_TEXTURE_2D);
      glColor4f (0, 0, 0, fade);
      glBegin (GL_QUADS);
      glVertex2i (0, 0);
      glVertex2i (0, state->render.render_height);
      glVertex2i (state->render.render_width, state->render.render_height);
      glVertex2i (state->render.render_width, 0);
      glEnd ();
    }
    if (TextureReady (state) && !EntityReady (state) && fade != 0.0f) {
      radius = state->render.render_width / 16;
      do_progress (state, (float)state->render.render_width / 2, (float)state->render.render_height / 2, (float)radius, fade, EntityProgress (state));
      RenderPrint (state, state->render.render_width / 2 - LOGO_PIXELS, state->render.render_height / 2 + LOGO_PIXELS, 0, glRgba (0.5f), "%1.2f%%", EntityProgress (state) * 100.0f);
      RenderPrint (state, 1, "%s v%d.%d.%03d", APP_TITLE, VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION);
    }
  }
  
  glPopMatrix ();
  glMatrixMode (GL_PROJECTION);
  glPopMatrix ();
  glMatrixMode (GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST);
  
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

int RenderMaxTextureSize (state_t* state)
{
  
  int mts;
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &mts);
  mts = MIN (mts, state->render.render_width);
  return MIN (mts, state->render.render_height);
  
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderPrint (state_t* state, int x, int y, int font, GLrgba color, const char *fmt, ...)				
{

  char		  text[MAX_TEXT];	
  va_list		ap;					
  
  text[0] = 0;
  if (fmt == NULL)			
		  return;						
  va_start(ap, fmt);		
  vsprintf(text, fmt, ap);				
  va_end(ap);		
  glPushAttrib(GL_LIST_BIT);				
  glListBase(fonts[font % FONT_COUNT].base_char - 32);				
  glColor3fv (&color.red);
	glRasterPos2i (x, y);
  glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderPrint (state_t* state, int line, const char *fmt, ...)				
{

  char		  text[MAX_TEXT];	
	va_list		ap;			
	
  text[0] = 0;	
  if (fmt == NULL)			
		  return;						
  va_start (ap, fmt);		
  vsprintf (text, fmt, ap);				
  va_end (ap);		
  glMatrixMode (GL_PROJECTION);
  glPushMatrix ();
  glLoadIdentity ();
  glOrtho (0, state->render.render_width, state->render.render_height, 0, 0.1f, 2048);
  glDisable(GL_DEPTH_TEST);
  glDepthMask (false);
	glMatrixMode (GL_MODELVIEW);
  glPushMatrix ();
  glLoadIdentity();
  glTranslatef(0, 0, -1.0f);				
  glDisable (GL_BLEND);
  glDisable (GL_FOG);
  glDisable (GL_TEXTURE_2D);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  RenderPrint (state, 0, line * FONT_SIZE - 2, 0, glRgba (0.0f), text);
  RenderPrint (state, 4, line * FONT_SIZE + 2, 0, glRgba (0.0f), text);
  RenderPrint (state, 2, line * FONT_SIZE, 0, glRgba (1.0f), text);
  glPopAttrib();						
  glPopMatrix ();
  glMatrixMode (GL_PROJECTION);
  glPopMatrix ();
  glMatrixMode (GL_MODELVIEW);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void static do_help (state_t* state)
{

  char*     text;
  int       line;
  char      parse[HELP_SIZE];
  int       x;
  
  strcpy (parse, help);
  line = 0;
  text = strtok (parse, "\n");
  x = 10;
  while (text) {
    RenderPrint (state, line + 2, text);
    text = strtok (NULL, "\n");
    line++;
  }

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderResize (state_t* state)		
{

  float     fovy;

  state->render.render_width = WinWidth (state);
  state->render.render_height = WinHeight (state);
  if (state->render.letterbox) {
    state->render.letterbox_offset = state->render.render_height / 6;
    state->render.render_height = state->render.render_height - state->render.letterbox_offset * 2;
  } else 
    state->render.letterbox_offset = 0;
  //render_aspect = (float)render_height / (float)render_width;
  glViewport (0, state->render.letterbox_offset, state->render.render_width, state->render.render_height);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  state->render.render_aspect = (float)state->render.render_width / (float)state->render.render_height;
  fovy = 60.0f;
  if (state->render.render_aspect > 1.0f) 
    fovy /= state->render.render_aspect; 
  gluPerspective (fovy, state->render.render_aspect, 0.1f, RENDER_DISTANCE);
	glMatrixMode (GL_MODELVIEW);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

#ifdef _WIN32
void RenderTerm (void)
{

  if (!hRC)
    return;
  wglDeleteContext (hRC);
  hRC = NULL;
}
#endif

#ifdef __APPLE__
void RenderTerm()
{
   // TOOD
}
#endif

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

#ifdef _WIN32
void RenderInit (void)
{

  HWND              hWnd;
	unsigned		      PixelFormat;
  HFONT	            font;		
	HFONT	            oldfont;

  hWnd = WinHwnd ();
  if (!(hDC = GetDC (hWnd))) 
		YOUFAIL ("Can't Create A GL Device Context.") ;
	if (!(PixelFormat = ChoosePixelFormat(hDC,&pfd)))
		YOUFAIL ("Can't Find A Suitable PixelFormat.") ;
  if(!SetPixelFormat(hDC,PixelFormat,&pfd))
		YOUFAIL ("Can't Set The PixelFormat.");
	if (!(hRC = wglCreateContext (hDC)))	
		YOUFAIL ("Can't Create A GL Rendering Context.");
  if(!wglMakeCurrent(hDC,hRC))	
		YOUFAIL ("Can't Activate The GL Rendering Context.");
  //Load the fonts for printing debug info to the window.
  for (int i = 0; i < FONT_COUNT; i++) {
	  fonts[i].base_char = glGenLists(96); 
	  font = CreateFont (FONT_SIZE,	0, 0,	0,	
				  FW_BOLD, FALSE,	FALSE, FALSE,	DEFAULT_CHARSET,	OUT_TT_PRECIS,		
				  CLIP_DEFAULT_PRECIS,	ANTIALIASED_QUALITY, FF_DONTCARE|DEFAULT_PITCH,
				  fonts[i].name);
	  oldfont = (HFONT)SelectObject(hDC, font);	
	  wglUseFontBitmaps(hDC, 32, 96, fonts[i].base_char);
	  SelectObject(hDC, oldfont);
	  DeleteObject(font);		
  } // end load fonts
  //If the program is running for the first time, set the defaults.
  if (!IniInt ("SetDefaults")) {
    IniIntSet ("SetDefaults", 1);
    IniIntSet ("Effect", EFFECT_BLOOM);
    IniIntSet ("ShowFog", 1);
  }
  //load in our settings
  letterbox = IniInt ("Letterbox") != 0;
  show_wireframe = IniInt ("Wireframe") != 0;
  show_fps = IniInt ("ShowFPS") != 0;
  show_fog = IniInt ("ShowFog") != 0;
  effect = IniInt ("Effect");
  flat = IniInt ("Flat") != 0;
  fog_distance = WORLD_HALF;
  //clear the viewport so the user isn't looking at trash while the program starts
  glViewport (0, 0, WinWidth (), WinHeight ());
  glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  SwapBuffers (hDC);
  RenderResize ();

}
#endif // ifdef _WIN32

#ifdef __APPLE__
void RenderInit(state_t* state)
{
//  HWND              hWnd;
//	unsigned		      PixelFormat;
//  HFONT	            font;		
//	HFONT	            oldfont;

/*  hWnd = WinHwnd ();
  if (!(hDC = GetDC (hWnd))) 
		YOUFAIL ("Can't Create A GL Device Context.") ;
	if (!(PixelFormat = ChoosePixelFormat(hDC,&pfd)))
		YOUFAIL ("Can't Find A Suitable PixelFormat.") ;
  if(!SetPixelFormat(hDC,PixelFormat,&pfd))
		YOUFAIL ("Can't Set The PixelFormat.");
	if (!(hRC = wglCreateContext (hDC)))	
		YOUFAIL ("Can't Create A GL Rendering Context.");
  if(!wglMakeCurrent(hDC,hRC))	
		YOUFAIL ("Can't Activate The GL Rendering Context.");
*/ // end basic OpenGL setup
/*
  //Load the fonts for printing debug info to the window.
  for (int i = 0; i < FONT_COUNT; i++) {
	  fonts[i].base_char = glGenLists(96); 
	  font = CreateFont (FONT_SIZE,	0, 0,	0,	
				  FW_BOLD, FALSE,	FALSE, FALSE,	DEFAULT_CHARSET,	OUT_TT_PRECIS,		
				  CLIP_DEFAULT_PRECIS,	ANTIALIASED_QUALITY, FF_DONTCARE|DEFAULT_PITCH,
				  fonts[i].name);
	  oldfont = (HFONT)SelectObject(hDC, font);	
	  wglUseFontBitmaps(hDC, 32, 96, fonts[i].base_char);
	  SelectObject(hDC, oldfont);
	  DeleteObject(font);		
  } // end load fonts
*/ // end load fonts
  //If the program is running for the first time, set the defaults.
  if (!IniInt ("SetDefaults")) {
    IniIntSet ("SetDefaults", 1);
    IniIntSet ("Effect", EFFECT_BLOOM);
    IniIntSet ("ShowFog", 1);
  }
  //load in our settings
  state->render.letterbox = IniInt ("Letterbox") != 0;
  state->render.show_wireframe = IniInt ("Wireframe") != 0;
  state->render.show_fps = IniInt ("ShowFPS") != 0;
  state->render.show_fog = IniInt ("ShowFog") != 0;
  state->render.effect = IniInt ("Effect");
  state->render.flat = IniInt ("Flat") != 0;
  state->render.fog_distance = WORLD_HALF;
  //clear the viewport so the user isn't looking at trash while the program starts
  glViewport (0, 0, WinWidth (state), WinHeight (state));
  glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//  SwapBuffers (hDC); TODO
  RenderResize (state);
}
#endif

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderFPSToggle (state_t* state)
{

  state->render.show_fps = !state->render.show_fps;
  IniIntSet ("ShowFPS", state->render.show_fps ? 1 : 0);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool RenderFog (state_t* state)
{

  return state->render.show_fog;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderFogToggle (state_t* state)
{

  state->render.show_fog = !state->render.show_fog;
  IniIntSet ("ShowFog", state->render.show_fog ? 1 : 0);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderLetterboxToggle (state_t* state)
{

  state->render.letterbox = !state->render.letterbox;
  IniIntSet ("Letterbox", state->render.letterbox ? 1 : 0);
  RenderResize (state);


}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderWireframeToggle (state_t* state)
{

  state->render.show_wireframe = !state->render.show_wireframe;
  IniIntSet ("Wireframe", state->render.show_wireframe ? 1 : 0);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool RenderWireframe (state_t* state)
{

  return state->render.show_wireframe;

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderEffectCycle (state_t* state)
{

  state->render.effect = (state->render.effect + 1) % EFFECT_COUNT;
  IniIntSet ("Effect", state->render.effect);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool RenderBloom (state_t* state)
{

  return state->render.effect == EFFECT_BLOOM || state->render.effect == EFFECT_BLOOM_RADIAL 
    || state->render.effect == EFFECT_DEBUG_OVERBLOOM || state->render.effect == EFFECT_COLOR_CYCLE;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool RenderFlat (state_t* state)
{

  return state->render.flat;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderFlatToggle (state_t* state)
{

  state->render.flat = !state->render.flat;
  IniIntSet ("Flat", state->render.flat ? 1 : 0);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderHelpToggle (state_t* state)
{

  state->render.show_help = !state->render.show_help;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

float RenderFogDistance (state_t* state)
{

  return state->render.fog_distance;

}

/*-----------------------------------------------------------------------------
  
  This is used to set a gradient fog that goes from camera to some portion of
  the normal fog distance.  This is used for making wireframe outlines and
  flat surfaces fade out after rebuild.  Looks cool.
  
-----------------------------------------------------------------------------*/

void RenderFogFX (state_t* state, float scalar)
{
  
  if (scalar >= 1.0f) {
    glDisable (GL_FOG);
    return;
  }
  glFogf (GL_FOG_START, 0.0f);
  glFogf (GL_FOG_END, state->render.fog_distance * 2.0f * scalar);
  glEnable (GL_FOG);
  
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void RenderUpdate (state_t* state)		
{

  GLvector        pos;
  GLvector        angle;
  GLrgba          color;
  int             elapsed;
  
  glViewport (0, 0, WinWidth (state), WinHeight (state));
  glDepthMask (true);
  glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
  glEnable(GL_DEPTH_TEST);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if (state->render.letterbox) 
    glViewport (0, state->render.letterbox_offset, state->render.render_width, state->render.render_height);
  
  if ( LOADING_SCREEN && TextureReady (state) && !EntityReady(state) ) {
    do_effects(state, EFFECT_NONE);
    return;
  }
  
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glShadeModel(GL_SMOOTH);
  glFogi (GL_FOG_MODE, GL_LINEAR);
  if (state->render.show_fog) {
    glEnable (GL_FOG);
    glFogf (GL_FOG_START, state->render.fog_distance - 100);
    glFogf (GL_FOG_END, state->render.fog_distance);
    color = glRgba (0.0f);
    glFogfv (GL_FOG_COLOR, &color.red);
  } else 
    glDisable (GL_FOG);
	glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);
  glEnable (GL_CULL_FACE);
  glCullFace (GL_BACK);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glMatrixMode (GL_TEXTURE);
  glLoadIdentity();
	glMatrixMode (GL_MODELVIEW);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glLoadIdentity();
  glLineWidth (1.0f);
  pos = CameraPosition (state);
  angle = CameraAngle (state);
  glRotatef (angle.x, 1.0f, 0.0f, 0.0f);
  glRotatef (angle.y, 0.0f, 1.0f, 0.0f);
  glRotatef (angle.z, 0.0f, 0.0f, 1.0f);
  glTranslatef (-pos.x, -pos.y, -pos.z);
  glEnable (GL_TEXTURE_2D);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  //Render all the stuff in the whole entire world.
  glDisable (GL_FOG);
  SkyRender (state);
  if (state->render.show_fog) {
    glEnable (GL_FOG);
    glFogf (GL_FOG_START, state->render.fog_distance - 100);
    glFogf (GL_FOG_END, state->render.fog_distance);
    color = glRgba (0.0f);
    color = glRgba (0.0f);
  }
  WorldRender (state);
  if (state->render.effect == EFFECT_GLASS_CITY) {
    glDisable (GL_CULL_FACE);
    glEnable (GL_BLEND);
    glBlendFunc (GL_ONE, GL_ONE);
    glDepthFunc (false);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode (GL_TEXTURE);
    glTranslatef ((pos.x + pos.z) / SEGMENTS_PER_TEXTURE, 0, 0);
	  glMatrixMode (GL_MODELVIEW);
  } else {
    glEnable (GL_CULL_FACE);
    glDisable (GL_BLEND);
  }
  EntityRender (state);
  if (!LOADING_SCREEN) {
    elapsed = 3000 - WorldSceneElapsed (state);
    if (elapsed >= 0 && elapsed <= 3000) {
      RenderFogFX (state, (float)elapsed / 3000.0f);
      glDisable (GL_TEXTURE_2D);
      glEnable (GL_BLEND);
      glBlendFunc (GL_ONE, GL_ONE);
      EntityRender (state);
    }
  }
  if (EntityReady (state))
    LightRender (state);
  CarRender (state);
  if (state->render.show_wireframe) {
    glDisable (GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    EntityRender (state);
  }
  do_effects (state, state->render.effect);
  //Framerate tracker
  if (GetTickCount () > state->render.next_fps) {
    state->render.current_fps = state->render.frames;
    state->render.frames = 0;
    state->render.next_fps = GetTickCount () + 1000;
  }
  if (state->render.show_fps)
    RenderPrint (state, 1, "FPS=%d : Entities=%d : polys=%d", state->render.current_fps, EntityCount (state) + LightCount (state) + CarCount (state), EntityPolyCount (state) + LightCount (state) + CarCount (state));
  //Show the help overlay
  if (state->render.show_help)
    do_help (state);
  state->render.frames++;

}
