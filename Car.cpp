/*-----------------------------------------------------------------------------

  Car.cpp

  2009 Shamus Young

-------------------------------------------------------------------------------

  This creates the little two-triangle cars and moves them around the map.

-----------------------------------------------------------------------------*/

#define DEAD_ZONE       200
#define STUCK_TIME      230
#define UPDATE_INTERVAL 50 //milliseconds
#define MOVEMENT_SPEED  0.61f
#define CAR_SIZE        3.0f

#ifdef _WIN32
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>
#else
#include <GLUT/GLUT.h>
#include <string.h>
#endif
#include <math.h>
#include "glTypes.h"

#include "Building.h"
#include "Car.h"
#include "Camera.h"
#include "Mesh.h"
#include "Macro.h"
#include "PixelMath.h"
#include "Random.h"
#include "Render.h"
#include "Texture.h"
#include "World.h"
#include "Visible.h"
#include "Win.h"

static GLvector           direction[] = 
{
  0.0f, 0.0f, -1.0f,
  1.0f, 0.0f,  0.0f,
  0.0f, 0.0f,  1.0f,
 -1.0f, 0.0f,  0.0f,
};

static int                dangles[] = { 0, 90, 180, 270};


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

int CarCount (state_t* state)
{

  return state->car.count;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CarClear (state_t* state)
{

  CCar*       c;

  for (c = state->car.head; c; c = c->m_next)
    c->Park ();
  memset(state->car.carmap, 0, sizeof (state->car.carmap));
  state->car.count = 0;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CarRender (state_t* state)
{

  CCar*       c;

  if (!state->car.angles_done) {
    for (int i = 0 ;i < 360; i++) {
      state->car.angles[i].x = cosf ((float)i * DEGREES_TO_RADIANS) * CAR_SIZE;
      state->car.angles[i].y = sinf ((float)i * DEGREES_TO_RADIANS) * CAR_SIZE;
    }
  }
  glDepthMask (false);
  glEnable (GL_BLEND);
  glDisable (GL_CULL_FACE);
  glBlendFunc (GL_ONE, GL_ONE);
  glBindTexture (GL_TEXTURE_2D, 0);
  glBindTexture(GL_TEXTURE_2D, TextureId (state, TEXTURE_HEADLIGHT));
  for (c = state->car.head; c; c = c->m_next)
    c->Render ();
  glDepthMask (true);

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CarUpdate (state_t* state)
{

  CCar*       c;
  unsigned    now;

  if (!TextureReady (state) || !EntityReady (state))
    return;
  now = GetTickCount ();
  if (state->car.next_update > now)
    return;
  state->car.next_update = now + UPDATE_INTERVAL;
  for (c = state->car.head; c; c = c->m_next)
    c->Update ();

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

CCar::CCar (state_t* state)
{
  
  m_state = state;
  m_ready = false;
  m_next = m_state->car.head;
  m_state->car.head = this;
  m_state->car.count++;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool CCar::TestPosition (int row, int col)
{

  //test the given position and see if it's already occupied
  if (m_state->car.carmap[row][col])
    return false;
  //now make sure that the lane is going the right direction
  if (WorldCell (m_state, row, col) != WorldCell (m_state, m_row, m_col))
    return false;
  return true;

}
 
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CCar::Update (void)
{

  int       new_row, new_col;
  GLvector  old_pos;    
  GLvector  camera;

  //If the car isn't ready, place it on the map and get it moving
  camera = CameraPosition (m_state);
  if (!m_ready) {
    //if the car isn't ready, we need to place it somewhere on the map
    m_row = DEAD_ZONE + RandomVal (m_state, WORLD_SIZE - DEAD_ZONE * 2);
    m_col = DEAD_ZONE + RandomVal (m_state, WORLD_SIZE - DEAD_ZONE * 2);
    //if there is already a car here, forget it.  
    if (m_state->car.carmap[m_row][m_col] > 0)
      return;
    //if this spot is not a road, forget it
    if (!(WorldCell (m_state, m_row, m_col) & CLAIM_ROAD)) 
      return;
    if (!Visible (m_state, glVector ((float)m_row, 0.0f, (float)m_col)))
      return;
    //good spot. place the car
    m_position = glVector ((float)m_row, 0.1f, (float)m_col);
    m_drive_position = m_position;
    m_ready = true;
    if (WorldCell (m_state, m_row, m_col) & MAP_ROAD_NORTH) 
      m_direction = NORTH;
    if (WorldCell (m_state, m_row, m_col) & MAP_ROAD_EAST) 
      m_direction = EAST;
    if (WorldCell (m_state, m_row, m_col) & MAP_ROAD_SOUTH) 
      m_direction = SOUTH;
    if (WorldCell (m_state, m_row, m_col) & MAP_ROAD_WEST) 
      m_direction = WEST;
    m_drive_angle = dangles[m_direction];
    m_max_speed = (float)(4 + RandomVal (m_state, 6)) / 10.0f;
    m_speed = 0.0f;
    m_change = 3;
    m_stuck = 0;
    m_state->car.carmap[m_row][m_col]++;
  }
  //take the car off the map and move it
  m_state->car.carmap[m_row][m_col]--;
  old_pos = m_position;
  m_speed += m_max_speed * 0.05f;
  m_speed = MIN (m_speed, m_max_speed);
  m_position += direction[m_direction] * MOVEMENT_SPEED * m_speed;
  //If the car has moved out of view, there's no need to keep simulating it. 
  if (!Visible (m_state, glVector ((float)m_row, 0.0f, (float)m_col))) 
    m_ready = false;
  //if the car is far away, remove it.  We use manhattan units because buildings almost always
  //block views of cars on the diagonal.
  if (fabs (camera.x - m_position.x) + fabs (camera.z - m_position.z) > RenderFogDistance (m_state))
    m_ready = false;
  //if the car gets too close to the edge of the map, take it out of play
  if (m_position.x < DEAD_ZONE || m_position.x > (WORLD_SIZE - DEAD_ZONE))
    m_ready = false;
  if (m_position.z < DEAD_ZONE || m_position.z > (WORLD_SIZE - DEAD_ZONE))
    m_ready = false;
  if (m_stuck >= STUCK_TIME)
    m_ready = false;
  if (!m_ready)
    return;
  //Check the new position and make sure its not in another car
  new_row = (int)m_position.x;
  new_col = (int)m_position.z;
  if (new_row != m_row || new_col != m_col) {
    //see if the new position places us on top of another car
    if (m_state->car.carmap[new_row][new_col]) {
      m_position = old_pos;
      m_speed = 0.0f;
      m_stuck++;
    } else {
      //look at the new position and decide if we're heading towards or away from the camera
      m_row = new_row;
      m_col = new_col;
      m_change--;
      m_stuck = 0;
      if (m_direction == NORTH)
        m_front = camera.z < m_position.z;
      else if (m_direction == SOUTH)
        m_front = camera.z > m_position.z;
      else if (m_direction == EAST)
        m_front = camera.x > m_position.x;
      else 
        m_front = camera.x < m_position.x;
    }
  }
  m_drive_position = (m_drive_position + m_position) / 2.0f;
  //place the car back on the map
  m_state->car.carmap[m_row][m_col]++;

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CCar::Render ()
{

  GLvector  pos;
  int       angle;
  int       turn;
  float     top;

  if (!m_ready)
    return;
  if (!Visible (m_state, m_drive_position))
    return;
  if (m_front) {
    glColor3f (1, 1, 0.8f);
    top = CAR_SIZE;
  } else {
    glColor3f (1, 0.2f, 0);
    top = 0.0f;
  }
  
  glBegin (GL_QUADS);

  angle = dangles[m_direction];
  pos = m_drive_position;// 
  angle = 360 - (int)MathAngle4 (m_position.x, m_position.z, pos.x, pos.z);
  angle %= 360;
  turn = (int)MathAngleDifference ((float)m_drive_angle, (float)angle);
  m_drive_angle += SIGN (turn);
  pos += glVector (0.5f, 0.0f, 0.5f);
  
  glTexCoord2f (0, 0);   
  glVertex3f (pos.x + m_state->car.angles[angle].x, -CAR_SIZE, pos.z + m_state->car.angles[angle].y);
  glTexCoord2f (1, 0);   
  glVertex3f (pos.x - m_state->car.angles[angle].x, -CAR_SIZE, pos.z - m_state->car.angles[angle].y);
  glTexCoord2f (1, 1);   
  glVertex3f (pos.x - m_state->car.angles[angle].x,  top, pos.z - m_state->car.angles[angle].y);
  glTexCoord2f (0, 1);   
  glVertex3f (pos.x + m_state->car.angles[angle].x,  top, pos.z +  m_state->car.angles[angle].y);
  
  glEnd ();

}