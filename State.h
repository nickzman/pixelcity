/*
 *  State.h
 *  PixelTesterApp
 *
 *  Created by Mikael Eiman on 2009-05-12.
 *  Copyright 2009 Electronic Magicians HB. All rights reserved.
 *
 */

#ifndef __STATE_H__
#define __STATE_H__

#ifdef OBJC_API_VERSION
#import <Cocoa/Cocoa.h>
#else
#define NSView void
#endif

#define WORLD_SIZE 1024
#define GRID_RESOLUTION   32
#define GRID_CELL         (GRID_RESOLUTION / 2)
#define GRID_SIZE         (WORLD_SIZE / GRID_RESOLUTION)
#define WORLD_TO_GRID(x)  (int)(x / GRID_RESOLUTION)
#define GRID_TO_WORLD(x)  ((float)x * GRID_RESOLUTION)

#include "glTypes.h"

class CSky;
class CCar;
class CLight;
class CEntity;
class CTexture;

typedef struct entity
{
  CEntity*            object;
} ;


typedef struct cell
{
  unsigned        list_textured;
  unsigned        list_flat;
  unsigned        list_flat_wireframe;
  unsigned        list_alpha;
  GLvector        pos;
} ;



typedef struct {
  GLrgba         bloom_color;
  long           last_update;
  char           world[WORLD_SIZE][WORLD_SIZE];
  CSky*          sky;
  int            fade_state;
  unsigned       fade_start;
  float          fade_current;
  int            modern_count;
  int            tower_count;
  int            blocky_count;
  bool           reset_needed;
  int            skyscrapers;
  GLbbox         hot_zone;
  int            logo_index;
  unsigned       start_time;
  int            scene_begin;
} app_state_t;


typedef struct {
  GLvector     angle;
  GLvector     position;
  GLvector     auto_angle;
  GLvector     auto_position;
  float        movement;
  bool         moving;
  bool         cam_auto;
  float        tracker;
  unsigned     last_update;
  int          camera_behavior;
} camera_state_t;

typedef struct {
  GLvector2          angles[360];
  bool               angles_done;
  unsigned char      carmap[WORLD_SIZE][WORLD_SIZE];
  CCar*              head;
  unsigned           next_update;
  int                count;
} car_state_t;

typedef struct {
  GLvector2      angles[5][360];
  CLight*        head;
  bool           angles_done;
  int            count;
} light_state_t;

typedef struct {
 cell           cell_list[GRID_SIZE][GRID_SIZE];
 int            entity_count;
 entity*        entity_list;
 bool           sorted;
 bool           compiled;
 int            polycount;
 int            compile_x;
 int            compile_y;
 int            compile_count;
 int            compile_end;
} entity_state_t;


typedef struct {
  CSky*          sky;
} sky_state_t;


#define PREFIX_COUNT 15
#define NAME_COUNT 15
#define SUFFIX_COUNT 15

typedef struct {
  CTexture*    head;
  bool         textures_done;
  bool         prefix_used[PREFIX_COUNT];
  bool         name_used[NAME_COUNT];
  bool         suffix_used[SUFFIX_COUNT];
  int          build_time;
} texture_state_t;

typedef struct {
  bool          vis_grid[GRID_SIZE][GRID_SIZE];
} visible_state_t;

typedef struct {
  float            render_aspect;
  float            fog_distance;
  int              render_width;
  int              render_height;
  bool             letterbox;
  int              letterbox_offset;
  int              effect;
  unsigned         next_fps;
  unsigned         current_fps;
  unsigned         frames;
  bool             show_wireframe;
  bool             flat;
  bool             show_fps;
  bool             show_fog;
  bool             show_help;
} render_state_t;

#define MAX_VBUFFER         256

typedef struct {
  GLvector         vector_buffer[MAX_VBUFFER];
} building_state_t;


#define N                     624

typedef struct {
  int              k;
  unsigned long    mag01[2];
  unsigned long    ptgfsr[N];
} random_state_t;

typedef struct {
  NSView* view;
  app_state_t app;
  camera_state_t camera;
  car_state_t car;
  light_state_t light;
  entity_state_t entity;
  sky_state_t sky;
  texture_state_t texture;
  visible_state_t visible;
  render_state_t render;
  building_state_t building;
  random_state_t random;
} state_t;

#endif
